const sqlite3 = require('sqlite3').verbose()

const db = new sqlite3.Database(':memory:')

db.serialize(function() {
  db.run(
    'CREATE TABLE installed (cloudId text, userId text, conversationId text)'
  )
})

module.exports = db
