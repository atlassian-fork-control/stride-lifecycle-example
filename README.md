# Stride Lifecycle events #

## About this app ##

This is an example app for Atlassian Stride.

This app shows how to hook up the Lifecycle events to listen for `installed` and `uninstalled` webhooks that are posted to your app.  When a user installs your app into a conversation, your app will receive an installation payload. In this sample app we'll show you which properties are important and how to save them off to a persistent data source.

## Set-up ##

### Prerequisites ###

* Make sure you have [Node.js 8.x](https://nodejs.org/) or above on your machine (You can run `node --version` in your terminal to check what version you have)
* Make sure you have the latest version of [ngrok](https://ngrok.com/) installed (you can check if you have it installed by running `ngrok --version` in your terminal)
* Sign up for an account on [https://developer.atlassian.com](https://developer.atlassian.com).  You can also log in with your Atlassian Account if you already have on.
* Once logged in, click on your avatar in the upper right corner and click [Create app](https://developer.atlassian.com/apps/create). Give your app a name, there are no wrong names. You can ignore description and icon at this point.
* In the next step you'll be asked to Enable an Atlassian API for your app, click on **Enable API** for the Stride API.
* Once your app is created you need to:
  * Go into the 'App Features' tab and ensure that the **Enabled a bot account** checkbox is checked.
  * Go into the 'Enabled APIs' tab and copy the Stride API Client Id and Client Secret, we'll need this later.

### Running the app ###

* Clone this repository to your local machine. Run this command in your terminal `git clone git@bitbucket.org:atlassian/stride-lifecycle-example.git`
* Open a terminal window and `cd` into your local repository `stride-lifecycle-example` and run ```npm install```.
* While that's installing we'll need to set our Client Id and Client Secret as environment variables. In the root of the repository you cloned, copy .env_sample into a file called .env and paste in your Client Id and Client Secret into the appropriate spots.
* Once npm finishes installing the node modules we can start up our app.  You can look in `package.json` for all the available scripts to run. We're going to start our app with logging turned on so we can see what's happening behind the scenes. Run `npm run verbose`.
* Now open a second terminal window and run ```ngrok http 3000```.  We need ngrok to expose our app running locally on our machine out to the internet so Stride can see it.
* Once ngrok starts up copy the https url from your ngrok output, this is where your app lives. You can get to the descriptor by navigating to the ngrok url + /descriptor (should look something like this `https://xxxxxxxx.ngrok.io/descriptor`)
* Go back to your app page on [https://developer.atlassian.com/apps](https://developer.atlassian.com/apps) and click on the app we created above
* Go into the 'Install' tab and paste your descriptor url in the *Descriptor Url* field.
* Click 'Refresh', you should see a green indicator
* Your app is now live and ready for use.

### Installing the app ###

* While you're still on the 'Install' tab, copy the **Installation URL**. This is the url we'll use to install the app in Stride.
* Go into a Stride room and click on the 'Apps' glance (the icon looks like a hexagon with a circle in the middle) on the right side of your screen, this will open up a sidebar.
* Click on the + icon at the top of the sidebar, this will open Strides app marketplace.
* Click on the 'Add custom app' link at the top of the page, this will open up a dialog where you can paste your installation URL. This will load your app's information into the dialog.
* Click 'Agree' to install the app.

### Seeing it in action ###

* In this sample app there are no visuals in Stride after you install.  All of the action is in the terminal where you ran `npm run verbose`
* In there you will see a bunch of logs. For instance, you should see that the server started, Stride called our `/installed` route and sent us in a payload and we can see that full payload in the terminal.
* In Stride, you can uninstall this app.  Click on the 'Apps' glance to pull up the sidebar.  This time click on the pencil and this will bring up all the apps that are installed in this chat as well as globally.  Pick the app you created and click on the three dots and click Remove from Conversation. And then confirm removal.
* This will fire the `/uninstalled` webhook.  You can go back to the terminal and watch the endpoint get called and the payload that gets passed in.

## The code ##

There are a lot of files in the repository so lets get you to the right place in the code to look at what's going on.

### `app-descriptor.json` ###

This file is what gets called when we tell developer.atlassian.com all about our app.  In here we need to declare where the `lifecycle` endpoints are for `installed` and `uninstalled`.

### `routes/index.js` ###

We're running a Node Express app and thus the routes for the lifecycle events are found in the routes folder.  In index.js we have a few routes.  One that handles the `/descriptor` and a couple for `/installed` and `uninstalled`

### `lib/jwt.js` ###

In this module we create a middleware function that we can use to validate the JWT token that is passed into our webhooks from Stride. By doing this we're able to secure our app and ensure that the request is coming from Stride.

### `lib/sqlite3.js` ###

In this module we're setting up an in-memory data store to use in this sample. If you were creating a production ready app you could put your data layer abstraction code in a module here.

We're only showing how you might go about storing data as there are many options available and we wanted to keep this sample light.

### Tests ###

There are a couple of provided tests in this repository.  We're using [jest](https://facebook.github.io/jest/) to run our tests. You can find the tests in the __tests__ folders.

We have tests for the descriptor and the index route file.

You can run the tests from your terminal by running `npm run test`.

## Getting help ##

Need help with this sample code or want to ask a question about Stride app development?  Head over to the [Atlassian Developer Community](https://community.developer.atlassian.com/) and create a new topic in the [Stride Development Category](https://community.developer.atlassian.com/c/stride-development).
